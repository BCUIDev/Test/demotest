//
//  AppDelegate.m
//  Test
//
//  Created by 陈黔 on 2020/4/24.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
	
		
	ViewController *homeVc = [[ViewController alloc] init];
	
	if ([homeVc isKindOfClass:[UIViewController class]]) {
		UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:(UIViewController*)homeVc];
		
		self.window.rootViewController = navCtrl;
	}
	[self.window makeKeyAndVisible];
	
	// Override point for customization after application launch.
	return YES;
}


@end
