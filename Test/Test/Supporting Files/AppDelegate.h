//
//  AppDelegate.h
//  Test
//
//  Created by 陈黔 on 2020/4/24.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;



@end

