//
//  ViewController.m
//  Test
//
//  Created by 陈黔 on 2020/4/24.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import "ViewController.h"
#import <PodTestOneKit/TTTViewController.h>

#define ZFBundle_Name @"BundleMacTest.bundle"
#define ZFBundle_Path [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:ZFBundle_Name]
#define ZFBundle [NSBundle bundleWithPath:ZFBundle_Path]

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	self.view.backgroundColor = UIColor.whiteColor;
	
	UIImageView *ivImageTest = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 200, 200)];
	ivImageTest.backgroundColor = UIColor.redColor;
	[self.view addSubview:ivImageTest];

//	1.外部导入的资源包
	ivImageTest.image = [UIImage imageNamed:@"bbbb" inBundle:[NSBundle bundleWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"BundleOutTest.bundle"]] compatibleWithTraitCollection:nil];
//	2.内部target资源包
	ivImageTest.image = [UIImage imageNamed:@"ios1" inBundle:ZFBundle compatibleWithTraitCollection:nil];
//	3.静态库framework中的bundle资源包,framework文件必须放到Copy Bundle Resources里面,不然读不到资源文件
	//长码
	ivImageTest.image = [UIImage imageNamed:@"imgtest" inBundle:[NSBundle bundleWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"PodTestOneKit.framework/KitImages.bundle"]] compatibleWithTraitCollection:nil];
	//代码分步
	NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"PodTestOneKit.framework/KitImages" ofType:@"bundle"];
	NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
	ivImageTest.image = [UIImage imageNamed:@"imgtest" inBundle:bundle withConfiguration:nil];

	
	UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
	btn.frame = CGRectMake(100, 300, 50, 20);
	[btn setTitle:@"点击" forState:UIControlStateNormal];
	[btn addTarget:self action:@selector(kkkk) forControlEvents:UIControlEventTouchUpInside];
	[self.view addSubview:btn];

}


- (void)kkkk{
	
	TTTViewController *vc = [[TTTViewController alloc] init];
	
	[self.navigationController pushViewController:vc animated:YES];
	
	vc.view.backgroundColor = UIColor.redColor;
	vc.title = @"TEst";
	
	
	
	NSLog(@"vcTTTT:%@", [vc returnAStriing:vc.title]);

	
}

@end
