import UIKit
// https://www.bilibili.com/video/BV144411C7Gg
// P49 访问权限
// P48 泛型的类型绑定 协议关联类型
protocol TestPro {
	associatedtype T:Any
	func test(p: T)
}

class TestPro1:TestPro{
	func test(p: Date) {
		print(p)
	}
}

let tttt = TestPro1()
tttt.test(p: Date())

	


// P46 异常
enum TestError: String, Error {
	case error1 = "Type1"
	case error2 = "Type2"
}

func play46 (p: Int) throws {
	if p < 0 {
		throw TestError.error1
	} else {
		throw TestError.error2
	}
}

do {
	try play46(p: 10)
} catch TestError.error1 {
	print("小于零")
} catch TestError.error2 {
	print("大于等于零")
}

// P45 尾随闭包
func play45(p1:String, p2:(String) -> Void ){
	p2(p1+"- Swift")
}
play45(p1: "Hello ", p2:{(p3: String) -> Void in
	print(p3)
})

func play451 (p: (String) -> String ) {
	let v = p("Swift1")
	print("返回值 ： "+v)
}
play451 { (p: String) -> String in
	return "Swift1"+p
}

play451 { _ in
	return "None"
}


// P39 可选初始化器
//var t:Student? = Student()
//if let p = t {
//	print(p.name)
//}
// P40 必要初始化器 闭包初始值
class Test40 {
	var name:String = {return "swift"}()
	var date:Date? = nil
	
	init(name:String, date:Date) {
		self.name = name
		self.date = date
	}
	
	required init(name:String) {
		self.name = name
	}
}

class TT:Test40 {
//	var ref:Test40? = nil // 强引用
//	weak var ref:Test40? = nil // 弱
	unowned var ref:Test40? = nil // 无主
	
	lazy var data:() -> Void = {[unowned self]() -> Void in
		print(self.name + "闭包循环引用")
	}
	
	deinit {
		print("TT 被销毁")
	}
}
// P41 反初始化器 强引用 ARC
var t1:TT? = TT(name: "强引用")
var t2 = t1
//t1 = nil
//t2 = nil

// P42-43 循环强引用 弱引用 无主引用
//t1?.ref = t2
//t2?.ref = t1
// 释放关联
//t1!.ref = nil
//t2!.ref = nil
// 释放
t1 = nil
t2 = nil
// P44 闭包循环引用 定义捕获列表
var t3:TT? = TT(name: "闭包循化引用")
t3!.data()

t3 = nil

// P45 可选链展开
var t4:TT? = TT(name: "可选链", date: Date.init())

t4 = nil

print(t4?.date ?? "None")


// P38 lazy 懒加载 什么时候使用什么时候初始化

// P26 类 属性计算 下标语法等和结构体类似
// P28 Any和AnyObject
// Any: 所有类型
// AnyObject: 类的类型
// P30 向下类型转换 父类类型的引用,指向子类类型的实例对象 1是Int类型，继承于Any类型,所以向下兼容
var m:Any = 1
// P31-32 重载 重写 final 禁止继承重写
// P34-36 继承 扩展 泛型
class Person {
	
}
// P37 协议
protocol Protocol1 {
	var value:String {get set}
	
	func fun1() -> String
}

class Worker: Person, Protocol1 {
	
	var value: String = ""
	
	func fun1() -> String {
		return "服你";
	}
}

extension Worker {
	func toString() {
		print("This id a toString")
	}
}

// P22-23 结构体
struct Student {
	var name = "unknown" {
		// P24 属性观察 监听
		willSet(new){
			print("willSet - " + new)
		}
		didSet(old){
			print("didSet - " + old)
		}
	}
	private var age = 0
	private var score = 0.0
	private var isPass = false
	// P25 下标语法subscript
	var scores = [100, 53, 88.0]
	subscript(index:Int, param:Double) -> Double{
		set{
			if index < scores.count {
				scores[index] = param
			} else {
				scores.append(param)
			}
		}
		get{
			return scores[scores.count > index ? index : scores.count - 1]
		}
	}
	
	static let schoolName = "家里蹲大学"//静态属性 由结构体名字调用
	
	//计算属性 属性在set get的时候进行变动 比如计算面积？
	//只读属性 只写get方法
	
	init() { //convenience 初始化调用初始化关键字,现在好像不用了
		self.init(name: "unkown", age: 0, score: 0)
	}
	
	init(name:String, age:Int, score:Double) {
		self.name = name
		self.age = age
		self.score = score
		
		self.isPass = self.score >= 60
	}
}

var a = Student()
var b = Student(name: "小步", age: 10, score: 66)
print(a.name, b)
b.name = "属性观察"
print(b.name)
b[3, 140.0] = 0
print(a.name, b)
print("下标语法 - " + String(b[3, 121.0]))


// P20-21 枚举
enum TestEnum1 {
	case name(String)
	case age(Int)
	case xy(Int, Int)
}

func play(param: TestEnum1) {
	switch param {
		case TestEnum1.name("Hello"):
			print("Hello")
		case TestEnum1.age(1):
			print(1)
		case TestEnum1.xy(10, 100):
			print(100, 200)
	
		default:
			print("None")
	}
}

play(param: TestEnum1.age(0))
play(param: TestEnum1.name("Hello"))

enum TestEnum: CaseIterable { //迭代器
	case A
	case B
	case C
}

for item in TestEnum.allCases {
	print(item)
}

func play (param: TestEnum) {
	if param == TestEnum.A {
		print("A")
	} else {
		print("NULL")
	}
}

play(param: TestEnum.A)



func test12(p: Bool) -> (Int) -> Int{
	func p1(value:Int) -> Int{
		return value+value
	}
	func p2(value:Int) -> Int{
		return value*value
	}

	return p ? p1 : p2
}

var v = test12(p: false)
print(v(3))


//
//var a = 10
//
//func test(v: inout Int) -> Int{
//	return 100 + v
//}
//test(v: &a)
//
//func test7() -> Void{
//	print("Test7")
//}
//var b:() -> Void = test7
//b()
//
//func test10(p:() -> Void){
//	p()
//}
//test10 {
//	test7()
//}
//
//func test11(p:(Int, Int) -> Int) -> Int{
//	return p(1, 2)
//}
//
//func add (a:Int, b:Int) -> Int{
//	return a+b;
//}
//
//var value = test11 { (a, b) -> Int in
//	return a+b;
//}
//print(value)
//
//
//
//var c:() -> Void = {() -> Void in
//	print("匿名函数")
//}
//c()
//
//func test8(param1: String, param2:Int) -> String{
//	return param1 + String(param2)
//}
//var d:(String, Int) -> String = test8(param1:param2:)
//print(d("Test8+", 9))
//
//var e:(String, Int) -> String = {(param1: String, param2:Int) -> String in
//	return param1 + String(param2)
//}
//print(e("Test8匿名函数", 8))
//
//var f:([Int]) -> String
//func test9(p:[Int]) -> String{
//	var t = ""
//
//	for item in p {
//		t += String(item)
//	}
//	return t
//}
//print(test9(p: [1,2,3]))
//
//var g:([Int]) -> String = { (p:[Int]) -> String in
//	var t = ""
//
//	for item in p {
//		t += String(item)
//	}
//	return t
//}
//print(g([1, 2, 3, 4]))

//func test1(name: String) -> String{
//	return name
//}
//
//func test3(name: String...) {
//	print(type(of: name))
//
//	for item in name {
//		print(type(of: item))
//	}
//}
//print(test3(name: "addd", "asddd"))
//
// 外部调用 内部调用 没有默认一样
//func test4(outname iname: String){
//	print(iname)
//}
//test4(outname: "Test4")
//
// 忽略外部name，直接传值 内部名称不能忽略
//func test5(_ iname: String){
//	print(iname)
//}
//test5("nil")
//
//func test6(name: String){
//	guard name == "1" else {
//		print("进入guard语句")
//
//		assert(false, "name为空")
//	}
//
//	print(name)
//}
//test6(name: "1")



//var str = "Hello, playground"
//
//print(Int("1")!+5)
//
//var a:String? = nil
//
//print(a)
//
//var b:(Int, String) = (1, "Test")
//
//var c:(index:Int, value:String) = (1, "Test")
//let (index, value) = (1, "Test")
//let (index1, _, value1) = (1, false, "Test")
//let (_, index2, value2) = (1, false, "Test")
//
//switch b {
//	case (1, "Test"):
//		print("T")
////		穿透
//		fallthrough
//	default:
//		print("无")
//}
//
//for index in stride(from: 1, to: 10, by: 2).reversed() {
//	print(index)
//}
//
//repeat{
//	a = a ?? "" + "asd";
//	print(a!)
//} while (!((a?.contains("asd"))!))
//
//print(str[str.startIndex])
//print(str[str.firstIndex(of: "p")!])
//print(str.prefix(4))
//print(str.suffix(3))
//
//var arr = ["A", "B", "C"]
//for item in arr[0...] {
//	print("arr:"+item)
//}
//
//var set = ["hello", "world", "swift"]
//print(set)
//
//var dic = ["a":"A", "b":"B", "c":"C"]
//let newDic = dic.filter { (key, value) -> Bool in
//	if key == "b" {
//		return false
//	}
//	return true
//}
//print(newDic)


