//
//  SceneDelegate.h
//  Test3
//
//  Created by BlackChen on 2020/6/12.
//  Copyright © 2020 BlackChen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

