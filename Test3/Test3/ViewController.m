//
//  ViewController.m
//  Test3
//
//  Created by BlackChen on 2020/6/12.
//  Copyright © 2020 BlackChen. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(50, 100, 100, 100)];
    view.backgroundColor = UIColor.blueColor;
    view.layer.shadowOffset = CGSizeMake(0, 3);
    view.layer.shadowOpacity = 0.1;
    [self.view addSubview:view];
    
    UIImageView *view1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    view1.backgroundColor = UIColor.whiteColor;
//    view1.layer.shadowOpacity = 0.8;
    [view addSubview:view1];
}


@end
