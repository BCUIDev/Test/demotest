//
//  BCTestModule.h
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface BCTestModule : NSObject<BHModuleProtocol>

@end

NS_ASSUME_NONNULL_END
