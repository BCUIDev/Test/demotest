//
//  ViewController.m
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import "TestViewController.h"

@interface TestViewController ()

@end

@implementation TestViewController

@synthesize strTitle = _strTitle;
@synthesize color = _color;

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	[self.view addSubview:self.btnClick];
	self.title = _strTitle;
	self.view.backgroundColor = _color;

	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"trade" style:UIBarButtonItemStylePlain target:self action:@selector(trade)];
}

- (void)trade{
	NSLog(@"trade");
	id <TradeServiceProtocol> obj = [[BeeHive shareInstance] createService:@protocol(TradeServiceProtocol)];
	if ([obj isKindOfClass:[UIViewController class]]) {
		obj.itemId = @"sadasdas";
		[self.navigationController pushViewController:(UIViewController *)obj animated:YES];
	}
}

- (void)click:(UIButton *)btn{
	NSLog(@"点击");
	id<ShopModuleServiceProtocol> obj = [[BeeHive shareInstance] createService:@protocol(ShopModuleServiceProtocol)];
	[[ShopModuleService alloc] nativeFetchDetailViewController:@{@"key":@"KKKK"}];

	if ([obj isKindOfClass:[UIViewController class]]) {
		obj.strTitle = @"测试233";
		[self.navigationController pushViewController:(UIViewController *)obj animated:YES];
	}
}

+ (BOOL)singleton{
	return NO;
}

// btn
- (UIButton *)btnClick{
	if (!_btnClick) {
		UIButton  *btnClick = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
		[btnClick setTitle:@"点我" forState:UIControlStateNormal];
		btnClick.backgroundColor = UIColor.redColor;
		[btnClick addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
		_btnClick = btnClick;
	}
	return _btnClick;
}

@end
