//
//  ViewController.h
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestViewController : UIViewController<BCTestServiceProtocol>

@property (nonatomic, strong) UIButton *btnClick;

@end

