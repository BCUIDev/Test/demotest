//
//  BCTestServiceProtocol.h
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BHServiceProtocol.h"

@protocol BCTestServiceProtocol <NSObject, BHServiceProtocol>

@property (nonatomic, copy) NSString *strTitle;//标题

@property (nonatomic, strong) UIColor *color;

@end
