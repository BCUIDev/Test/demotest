//
//  BHTradeViewController.m
//  BeeHive
//
//  Created by 一渡 on 7/14/15.
//  Copyright (c) 2015 一渡. All rights reserved.
//

#import "BHTradeViewController.h"
#import <PodTestOneKit/PodTestOneKit.h>
@implementation BHTradeViewController

@synthesize itemId = _itemId;

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame)-100, 0, 200, 300)];
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = [NSString stringWithFormat:@"%@", self.itemId];
    
    [self.view addSubview:label];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.frame)-50,
                                                               CGRectGetMaxY(label.frame)-20,
                                                               100,
                                                               80)];
    btn.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    btn.backgroundColor = [UIColor blackColor];
    
    [btn setTitle:@"点我" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
//	UIImage *image = [UIImage imageNamed:@"imgtest" inBundle:[NSBundle bundleWithPath:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Pods_BHTest.framework/KitImages.bundle"]] compatibleWithTraitCollection:nil];
	
//	[btn setImage:image forState:UIControlStateNormal];

    [self.view addSubview:btn];
}

-(void)click:(UIButton *)btn{
//	Framework
	TTTViewController *vc = [[TTTViewController alloc] init];
	[self.navigationController pushViewController:vc animated:YES];
	
	vc.view.backgroundColor = UIColor.redColor;
	vc.title = @"私有库调用";
	
	NSLog(@"vcTTTT:%@", [vc returnAStriing:vc.title]);
	
	NSLog(@"vcTTTT:%@", [vc returnAStriing1:vc.title]);

	
//	BeeHive
//    id<TradeServiceProtocol> obj = [[BeeHive shareInstance] createService:@protocol(TradeServiceProtocol)];
//    if ([obj isKindOfClass:[UIViewController class]]) {
//        obj.itemId = [NSString stringWithFormat:@"%d", arc4random()];
//        [self.navigationController pushViewController:(UIViewController *)obj animated:YES];
//    }
}


@end
