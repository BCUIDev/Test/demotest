//
//  BCAppDelegate.h
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import "BHAppDelegate.h"


NS_ASSUME_NONNULL_BEGIN

@interface BCAppDelegate : BHAppDelegate <UIApplicationDelegate>


@end

NS_ASSUME_NONNULL_END
