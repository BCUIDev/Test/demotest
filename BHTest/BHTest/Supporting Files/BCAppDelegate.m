//
//  BCAppDelegate.m
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import "BCAppDelegate.h"
#import "BCTestServiceProtocol.h"

@implementation BCAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary<UIApplicationLaunchOptionsKey,id> *)launchOptions{
	self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];


	[BHContext shareInstance].env = BHEnvironmentDev;
	[BHContext shareInstance].application = application;
	[BHContext shareInstance].launchOptions = launchOptions;
	[BHContext shareInstance].moduleConfigName = @"TestBeeHive";
	[BHContext shareInstance].serviceConfigName = @"TestBHService";
	[BeeHive shareInstance].enableException = YES;
	[[BeeHive shareInstance] setContext:[BHContext shareInstance]];

	[super application:application didFinishLaunchingWithOptions:launchOptions];

	id<BCTestServiceProtocol> homeVc = [[BeeHive shareInstance] createService:@protocol(BCTestServiceProtocol)];

	if ([homeVc isKindOfClass:[UIViewController class]]) {
		homeVc.strTitle = @"标题1";
		homeVc.color = UIColor.yellowColor;
		UINavigationController *navCtrl = [[UINavigationController alloc] initWithRootViewController:(UIViewController*)homeVc];
		
		self.window.rootViewController = navCtrl;
	}
	[self.window makeKeyAndVisible];

	return YES;
}

@end
