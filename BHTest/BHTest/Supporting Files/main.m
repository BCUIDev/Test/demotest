//
//  main.m
//  BHTest
//
//  Created by 陈黔 on 2020/4/26.
//  Copyright © 2020 BCSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCAppDelegate.h"

int main(int argc, char * argv[]) {
	NSString * appDelegateClassName;
	@autoreleasepool {
	    // Setup code that might create autoreleased objects goes here.
	    appDelegateClassName = NSStringFromClass([BCAppDelegate class]);
	}
	return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
