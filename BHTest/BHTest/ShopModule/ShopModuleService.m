//
//  ShopTarget.m
//  BeeHive
//
//  Created by DP on 16/3/28.
//  Copyright © 2016年 一渡. All rights reserved.
//

#import "ShopModuleService.h"
#import "ShopModuleViewController.h"

typedef void (^CTUrlRouterCallbackBlock)(NSDictionary *info);
@interface ShopModuleService (){
	ShopModuleViewController *_vc;
}
@end

@implementation ShopModuleService

- (UIViewController *)nativeFetchDetailViewController:(NSDictionary *)params{
    // 因为action是从属于ModuleA的，所以action直接可以使用ModuleA里的所有声明
	self.vc.valueLabel.text = params[@"key"];
	self.vc.title = params[@"title"];
    return self.vc;
}

- (id)nativePresentImage:(NSDictionary *)params{
    self.vc.valueLabel.text = @"this is image";
    self.vc.imageView.image = params[@"image"];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:self.vc animated:YES completion:nil];
    return nil;
}

- (id)showAlert:(NSDictionary *)params{
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        CTUrlRouterCallbackBlock callback = params[@"cancelAction"];
        if (callback) {
            callback(@{@"alertAction":action});
        }
    }];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        CTUrlRouterCallbackBlock callback = params[@"confirmAction"];
        if (callback) {
            callback(@{@"alertAction":action});
        }
    }];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"alert from Module A" message:params[@"message"] preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:cancelAction];
    [alertController addAction:confirmAction];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
    return nil;
}

- (id)nativeNoImage:(NSDictionary *)params{
    self.vc.valueLabel.text = @"no image";
    self.vc.imageView.image = [UIImage imageNamed:@"noImage"];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:self.vc animated:YES completion:nil];
    
    return nil;
}

//如果没有找到对应Action，会走到这边
- (id)notFound:(NSDictionary *)params{
    NSLog(@"sorry, we have find this action");
    return nil;
}

- (ShopModuleViewController *)vc{
	if (!_vc) {
		_vc = [[ShopModuleViewController alloc]init];
		
	}
	return _vc;
}

@end
