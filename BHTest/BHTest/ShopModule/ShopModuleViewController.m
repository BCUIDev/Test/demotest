//
//  ShopModuleViewController.m
//  BeeHive
//
//  Created by DP on 16/3/28.
//  Copyright © 2016年 一渡. All rights reserved.
//

#import "ShopModuleViewController.h"
#import "BCTestTool.h"
@interface ShopModuleViewController()<ShopModuleServiceProtocol>

@property (nonatomic, strong, readwrite) UILabel *valueLabel;
@property (nonatomic, strong, readwrite) UIImageView *imageView;

@property (nonatomic, strong) UIButton *returnButton;

@end

@implementation ShopModuleViewController

- (void)nativeFetchDetailViewController:(NSDictionary *)params{
	self.valueLabel.text = params[@"key"];

}

@synthesize strTitle = _strTitle;
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
	
	BCTestTool *test = [[BCTestTool alloc]init];
	[test doBCTest];
	
    self.view.backgroundColor = [UIColor grayColor];

	self.title = _strTitle;
    [self.view addSubview:self.valueLabel];
    [self.view addSubview:self.imageView];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.returnButton];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.valueLabel sizeToFit];
}

#pragma mark - event response
- (void)didTappedReturnButton:(UIButton *)button{
	NSLog(@"返回");
    if (self.navigationController == nil) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - getters and setters
- (UILabel *)valueLabel{
    if (_valueLabel == nil) {
        _valueLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 100, 100, 40)];
        _valueLabel.font = [UIFont systemFontOfSize:30];
        _valueLabel.textColor = [UIColor blackColor];
		_valueLabel.text = @"价值";
    }
    return _valueLabel;
}

- (UIImageView *)imageView{
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
		_imageView.backgroundColor = UIColor.whiteColor;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

- (UIButton *)returnButton{
    if (_returnButton == nil) {
        _returnButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_returnButton addTarget:self action:@selector(didTappedReturnButton:) forControlEvents:UIControlEventTouchUpInside];

        [_returnButton setTitle:@"return" forState:UIControlStateNormal];
        [_returnButton setTitleColor:[UIColor brownColor] forState:UIControlStateNormal];
    }
    return _returnButton;
}


@end
